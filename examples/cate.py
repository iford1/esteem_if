from esteem import drivers
from esteem import parallel
from esteem.wrappers import nwchem, amber, onetep

# List solutes and solvents and get default arguments
all_solutes = {'cate': 'catechol'}
all_solvents = {'cycl': 'cyclohexane', 'meth': 'methanol'}
solutes_args, solvate_args, clusters_args, spectra_args = drivers.get_default_args()

# Some simple overrides for a quick job
solutes_args.basis = '6-31G'
solutes_args.func = 'PBE'
solutes_args.directory = 'PBE'
solvate_args.boxsize = 18
solvate_args.ewaldcut = 10
solvate_args.nsnaps = 20
clusters_args.radius = 3

# Setup parallel execution of tasks
make_script = parallel.make_sbatch
script_settings = parallel.athena_1node
clus_script_settings = parallel.athena_1node
solutes_wrapper = nwchem.NWChemWrapper()
solvate_wrapper = amber.AmberWrapper()
clusters_wrapper = onetep.OnetepWrapper()
solutes_wrapper.nwchem_setup()

# Run main driver
drivers.main(all_solutes,all_solvents,i
             solutes_args,solvate_args,clusters_args,spectra_args,
             solutes_wrapper,solvate_wrapper,clusters_wrapper,
             make_script,solutes_script_settings,solvate_script_settings,
             clusters_script_settings)
exit()

