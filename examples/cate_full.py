from esteem.tasks import drivers
from esteem.wrappers import nwchem
from esteem.wrappers import amber

# Setup solute and solvents
all_solutes = {'cate': 'catechol'}
all_solvents = {'cycl': 'cyclohexane', 'meth': 'methanol'}

# Get default arguments
solutes_args, solvate_args, clusters_args, spectra_args = Drivers.get_default_args()
solutes_args.basis = '6-311++G**'

# Set solutes args
all_solutes_args = {}
for func in ['PBE','PBE0']:
   all_solutes_args[func] = deepcopy(solutes_args)
   all_solutes_args[func].func = func
   all_solutes_args[func].directory = func

# Set solvate args
solvate_args.boxsize = 15

# Set clusters args
clusters_args.output = 'onetep'
all_clusters_args = {}
for rad in [0,3,6,9]:
   target = f'solvR{rad}'
   all_clusters_args[target] = deepcopy(clusters_args)
   all_clusters_args[target].radius = rad
   all_clusters_args[target].exc_suffix = target

# Set spectra args
spectra_args.exc_suffix    = 'solvR6'
spectra_args.broad         = 0.05 # eV
spectra_args.wavelength    = [300,800,1] # nm
spectra_args.warp_origin_prefix = 'PBE/is_tddft'
spectra_args.warp_dest_prefix   = 'PBE0/is_tddft'

# Setup code wrappers
nwchem.nwchem_setup()
solutes_funcs = nwchem.funcs # use NWChem for the Solutes task
solvate_funcs = amber.funcs  # use Amber for the Solvate task

# Setup parallelism
make_script = drivers.make_sbatch       # use SLURM
script_settings = drivers.nanosim_1node # use parallel settings for 1 nanosim node
clus_script_settings = drivers.nanosim_1node

# Invoke main driver
drivers.main(all_solutes,all_solvents,all_solutes_args,solvate_args,all_clusters_args,spectra_args,
             solutes_funcs,solvate_funcs,make_script,script_settings,clus_script_settings)

# Quit - function defs for interactive use might follow
exit()

