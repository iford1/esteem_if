.. _spectra:

Spectra module
==============

Python library usage
--------------------

.. automodule:: esteem.tasks.spectra
   :members:

Command-line usage
------------------

.. argparse::
   :module: esteem.tasks.spectra
   :func: make_parser
   :prog: spectra


