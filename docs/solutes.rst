.. _solutes:

Solutes module
==============

Python library usage
--------------------

.. automodule:: esteem.tasks.solutes
   :members:

Command line usage
------------------

.. argparse::
   :module: esteem.tasks.solutes
   :func: make_parser
   :prog: solutes


