.. _solvate:

Solvate module
==============

Python library usage
--------------------

.. automodule:: esteem.tasks.solvate
   :members:

Command-line usage
------------------

.. argparse::
   :module: esteem.tasks.solvate
   :func: make_parser
   :prog: solvate


