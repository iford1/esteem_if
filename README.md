ESTEEM: Explicit Solvent Toolkit for Electronic Excitations in Molecules
========================================================================

## Contents of package:

A set of Jupyter Notebooks, which compile to python scripts
that use the Atomic Simulation Environment python packages,
plus a set of atomistic modelling packages.

## Dependencies:

* Python 3
* ASE (tested on 3.19.0)
* NWChem >=6.6 (tested on 6.6, 6.8, 7.0)
* AMBER (tested on AmberTools 16.0 and 20.0)
* ONETEP (tested on 5.3.x and 6.0)
* AMP (not currently core functionality)

## License:

This package is distributed under the MIT License.

## Setup:

Extract the package and run the ipynb -> py conversion by
typing "python setup.py". This is also a test of whether your
environment has everything required.

You will need to add the directory containing the scripts to
your PYTHONPATH. For example, in .bashrc you could add a line
such as

PYTHONPATH=$PYTHONPATH:~/esteem

ONETEP: if ONETEP is uesd, the code assumes it will find a binary
by the name "onetep" in $PATH or as an alias.

AMBER Tools bin directory is assumed to be in $PATH (as AMBER
installation recommends) - the code will use sander.MPI if
available, otherwise sander. Also uses antechamber, cpptraj,
tleap, and various other utilities.

NWChem: the code assumes it will find a binary by the name "nwchem"

Calculation choices are currently a mixture of some hard-wired,
some user-modifiable. The DFT parameters and those associated
with setup of the solvent box or cluster size can be tweaked
script, but some of the MD parameters are hard-wired into
Amber.ipynb - edit those directly and/or let me know what
most needs to be on-the-fly editable.

